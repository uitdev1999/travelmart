import 'package:flutter/material.dart';
import 'package:travelmart/pages/profile/components/sidebarProfile.dart';
import 'package:travelmart/widget/topBar.dart';
class ProfilePage extends StatefulWidget{
  ProfilePage ({Key ? key}) : super(key: key);
  @override
   _ProfilePageState createState() => _ProfilePageState();
}
class _ProfilePageState extends State<ProfilePage>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Stack(
        alignment: Alignment.topLeft,
        children: [
        Container(
        height: 60.0,
        child: Material(
          elevation: 0.0,
          child: TopBar(),
        ),
          ),
        ],
      ),
    );
  }
}
