import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travelmart/pages/details/components/imagesDetail.dart';
import'package:travelmart/widget/topBar.dart';
class Detail extends StatefulWidget{
  Detail({Key? key}) : super (key: key);
  @override
  _DetailState createState() => _DetailState();
}
class _DetailState extends State<Detail>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
       body: Container(
         child: Stack(
           alignment: Alignment.topLeft,
           children: [
               Scrollbar(
                 isAlwaysShown: true,
                   child: SingleChildScrollView(
                     child: Column(
                       mainAxisSize: MainAxisSize.max,
                       children: [
                         SizedBox(height: 100,),
                         Row(
                             crossAxisAlignment:  CrossAxisAlignment.center,
                           children: [
                             Text("abcd"),
                             ImagesDetail(),
                           ],
                         )
                       ],
                     ),
                   ),
               ),
             Container(
               height: 80.0,
               child: Material(
                 elevation: 0.0,
                 child: TopBar(),
               ),
             ),

           ],
         ),
       ),
    );
  }
}