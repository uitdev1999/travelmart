import 'package:ant_icons/ant_icons.dart';
import 'package:flutter/material.dart';
class Products extends StatefulWidget {
  Products({Key? key}) : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    var largeScreenGrid = currentWidth > 1366;
    var smallScreenGrid = currentWidth > 1201;
    var tabScreenGrid = currentWidth > 769;

    return Container(
      margin: EdgeInsets.only(
          left: largeScreenGrid
              ? 100.0
              : smallScreenGrid
                  ? 40.0
                  : tabScreenGrid
                      ? 10.0
                      : 10.0,
          top: smallScreenGrid ? 30.0 : 15.0,
          right: largeScreenGrid
              ? 100.0
              : smallScreenGrid
                  ? 40.0
                  : tabScreenGrid
                      ? 10.0
                      : 10.0,
          bottom: 0.0),
      child: Scrollbar(
        child: GridView.count(
          crossAxisCount: smallScreenGrid ? 3 : 3,
          childAspectRatio: 01.48,
          mainAxisSpacing: smallScreenGrid ? 30.0 : 10.0,
          crossAxisSpacing: smallScreenGrid ? 30.0 : 10.0,
          shrinkWrap: true,
          physics: const ScrollPhysics(),
          children: [
            Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/07/22/09/50/rice-noodles-855077_640.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                   Container(
                          margin: EdgeInsets.all(20.0),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              elevation: 0.0,
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(0.0),
                              ),
                              padding: EdgeInsets.all(18.0),
                            ),
                            onPressed: () {},
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: smallScreenGrid ?  30.0 : 10.0,
                                  top: 0.0,
                                  right: smallScreenGrid ? 30.0 : 10.0 ,
                                  bottom: 0.0),
                              child: Text(
                                'Thực phẩm',
                                style: TextStyle(color: Colors.black, fontSize: smallScreenGrid ? 30 :10,),
                              ),
                            ),
                          ),
                        )

                ],
              ),
            ),

            Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2019/11/30/21/37/stars-4664313_640.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),

                      Container(
                          margin: EdgeInsets.all(20.0),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              elevation: 0.0,
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(0.0),
                              ),
                              padding: EdgeInsets.all(18.0),
                            ),
                            onPressed: () {},
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: smallScreenGrid ?  30.0 : 8.0,
                                  top: 0.0,
                                  right: smallScreenGrid ?  30.0 : 8.0,
                                  bottom: 0.0),
                              child: Text(
                                'Quà lưu niệm',
                                style: TextStyle(color: Colors.black,fontSize: smallScreenGrid ?  30.0 : 10.0),
                              ),
                            ),
                          ),
                        )

                ],
              ),
            ),
            Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/12/15/18/store-984393_640.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),

                       Container(
                          margin: EdgeInsets.all(20.0),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              elevation: 0.0,
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(0.0),
                              ),
                              padding: EdgeInsets.all(18.0),
                            ),
                            onPressed: () {},
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: smallScreenGrid ?  30.0 : 10.0,
                                  top: 0.0,
                                  right: smallScreenGrid ?  30.0 : 10.0,
                                  bottom: 0.0),
                              child: Text(
                                'Khác',
                                style: TextStyle(color: Colors.black,fontSize: smallScreenGrid ?  30.0 : 10.0),
                              ),
                            ),
                          ),
                        )

                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
