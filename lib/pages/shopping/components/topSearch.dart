import 'package:flutter/material.dart';

class TopSearch extends StatefulWidget {
  TopSearch({Key? key}) : super(key: key);

  @override
  _TopSearchState createState() => _TopSearchState();
}

class _TopSearchState extends State<TopSearch> {
  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    var extraLargeScreenGrid = currentWidth > 1536;
    var largeScreenGrid = currentWidth > 1366;
    var smallScreenGrid = currentWidth > 1201;
    var tabScreenGrid = currentWidth > 769;

    return Container(
      padding: EdgeInsets.only(
          left: largeScreenGrid
              ? 100.0
              : smallScreenGrid
              ? 40.0
              : tabScreenGrid
              ? 10.0
              : 10.0,
          top: 0.0,
          right: largeScreenGrid
              ? 100.0
              : smallScreenGrid
              ? 40.0
              : tabScreenGrid
              ? 10.0
              : 10.0,
          bottom: 0.0),
      child: GridView.count(
        crossAxisCount: smallScreenGrid ? 5:3,
        childAspectRatio: 0.7,
        mainAxisSpacing: smallScreenGrid ? 10.0 : 10.0,
        crossAxisSpacing: smallScreenGrid ? 10.0 : 10.0,
        shrinkWrap: true,
        children: [
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },

              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },

              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {

              },
              child: Column(
                children: [
                  Expanded(
                    child: Image.network(
                      'https://cdn.pixabay.com/photo/2015/10/30/18/35/shoes-1014606_960_720.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: smallScreenGrid ? 100.0 : 50.0,
                        padding: EdgeInsets.only(top: 08.0),
                        child: Text(
                          "Giày",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: smallScreenGrid ? 12.0 : 8.0),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text("5"),
                            Icon(Icons.star,size: 15,),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(top: 05.0, bottom: 08.0),
                    child: Text(
                      "\$10",
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
