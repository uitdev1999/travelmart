
import 'package:flutter/material.dart';
import 'package:travelmart/pages/shopping/components/bestSelling.dart';
import 'package:travelmart/widget/bottomBar.dart';
import 'package:travelmart/pages/shopping/components/newArrival.dart';
import 'package:travelmart/pages/shopping/components/products.dart';
import 'package:travelmart/widget/topBar.dart';
import 'package:travelmart/pages/shopping/components/topSearch.dart';

class ShoppingPage extends StatefulWidget {
  ShoppingPage({Key? key}) : super(key: key);

  @override
  _ShoppingPageState createState() => _ShoppingPageState();
}

class _ShoppingPageState extends State<ShoppingPage> {
  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    var smallScreenGrid = currentWidth > 1201;

    return Scaffold(
      body: Container(
        child: Stack(
          alignment: Alignment.topLeft,
          children: [
            Scrollbar(
              isAlwaysShown: true,
              child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(height: smallScreenGrid ? 40:70,),
                  Container(
                    margin: EdgeInsets.only(
                        top: smallScreenGrid ? 70.0 : 40.0,
                        bottom: smallScreenGrid ? 70.0 : 40.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Text(
                            'Danh mục',
                            style: TextStyle(
                                fontSize: smallScreenGrid ? 30.0 : 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                  Products(),
                  Container(
                    margin: EdgeInsets.only(
                        top: smallScreenGrid ? 70.0 : 40.0,
                        bottom: smallScreenGrid ? 70.0 : 40.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Text(
                            'Bán chạy',
                            style: TextStyle(
                                fontSize: smallScreenGrid ? 30.0 : 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                  BestSelling(),
                  Container(
                    margin: EdgeInsets.only(top: 40.0, bottom: 40.0),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.indigo[600],
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          padding: EdgeInsets.all(18.0),
                        ),
                        onPressed: () {},
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 30.0, top: 0.0, right: 30.0, bottom: 0.0),
                          child: Text(
                            'Xem tất cả',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(
                        top: smallScreenGrid ? 50.0 : 30.0,
                        bottom: smallScreenGrid ? 50.0 : 50.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Text(
                            'Sản phẩm mới',
                            style: TextStyle(
                                fontSize: smallScreenGrid ? 30.0 : 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                  NewArrival(),
                  Container(
                    margin: EdgeInsets.only(top: 40.0, bottom: 40.0),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.indigo[600],
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          padding: EdgeInsets.all(18.0),
                        ),
                        onPressed: () {},
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 30.0, top: 0.0, right: 30.0, bottom: 0.0),
                          child: Text(
                            'Xem tất cả',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: smallScreenGrid ? 50.0 : 30.0,
                        bottom: smallScreenGrid ? 50.0 : 50.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Text(
                            'Tìm kiếm hàng đầu',
                            style: TextStyle(
                                fontSize: smallScreenGrid ? 30.0 : 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: smallScreenGrid ? 2.0 : 1.0,
                          width: smallScreenGrid ? 150.0 : 80.0,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                  TopSearch(),
                  Container(
                    margin: EdgeInsets.only(top: 40.0, bottom: 40.0),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.indigo[600],
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          padding: EdgeInsets.all(18.0),
                        ),
                        onPressed: () {},
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 30.0, top: 0.0, right: 30.0, bottom: 0.0),
                          child: Text(
                            'Xem tất cả',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 100.0),
                  BottomBar(),
                ],
              ),
            ),
            ),
            Container(
              height: 80.0,
              child: Material(
                elevation: 0.0,
                child: TopBar(),
              ),
            ),

          ],
        ),
      ),

    );
  }
}
