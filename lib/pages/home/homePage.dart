
import 'package:flutter/material.dart';
import 'package:travelmart/widget/bottomBar.dart';
import 'package:travelmart/pages/home/components/slideImages.dart';
import 'package:travelmart/widget/topBar.dart';


class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          alignment: Alignment.topLeft,
          children: [
            Scrollbar(
              isAlwaysShown: true,
              child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SlideImages(),
                  SizedBox(height: 100.0),
                  BottomBar(),
                ],
              ),
            ),
            ),

            Container(
              height: 80.0,
              child: Material(
                elevation: 0.0,
                child: TopBar(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
