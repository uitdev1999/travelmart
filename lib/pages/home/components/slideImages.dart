import 'dart:ui';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:travelmart/pages/login_register/loginPage.dart';

class SlideImages extends StatefulWidget {
  SlideImages({Key? key}) : super(key: key);

  @override
  _SlideImagesState createState() => _SlideImagesState();
}

class _SlideImagesState extends State<SlideImages> {
  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    var extraLargeScreenGrid = currentWidth > 1536;
    var largeScreenGrid = currentWidth > 1366;
    var smallScreenGrid = currentWidth > 1201;
    var tabScreenGrid = currentWidth > 769;
    return Container(
      margin: EdgeInsets.only(
          left: smallScreenGrid ? 0.0 : 10.0,
          top: 0.0,
          right: smallScreenGrid ? 0.0 : 10.0,
          bottom: 0.0),
      child: GridView.count(
        crossAxisCount: 1,
        childAspectRatio: extraLargeScreenGrid
            ? 2.5
            : largeScreenGrid
                ? 2.1
                : smallScreenGrid
                    ? 2.3
                    : tabScreenGrid
                        ? 1.8
                        : 1.5,
        mainAxisSpacing: 0.0,
        crossAxisSpacing: 0.0,
        shrinkWrap: true,
        children: [
          Stack(
            alignment: Alignment.centerLeft,
            children: [
              Carousel(
                boxFit: BoxFit.cover,
                autoplay: true,
                animationCurve: Curves.fastOutSlowIn,
                animationDuration: Duration(milliseconds: 1500),
                dotSize: smallScreenGrid ? 12.0 : 08.0,
                dotIncreaseSize: 01.4,
                dotIncreasedColor: Colors.green ,
                dotColor: Colors.white,
                dotBgColor: Colors.transparent,
                dotPosition: DotPosition.bottomCenter,
                dotVerticalPadding: 10.0,
                showIndicator: true,
                indicatorBgPadding: 7.0,
                dotHorizontalPadding: 40,

                images: [
                  NetworkImage(
                      'https://th.bing.com/th/id/R.5d9b909ed5da85f5f4d71e49f0644341?rik=I2vXgRTVk%2biBow&pid=ImgRaw&r=0'),
                  NetworkImage(
                      'https://cdn.pixabay.com/photo/2019/04/30/10/31/sea-4168234_960_720.jpg'),
                  NetworkImage(
                      'https://cdn.pixabay.com/photo/2020/03/25/02/31/rolls-4965915_960_720.jpg'),
                  NetworkImage(
                      'https://cdn.pixabay.com/photo/2021/03/28/03/56/viet-phuc-6130178_960_720.jpg'),
                  NetworkImage(
                      'https://cdn.pixabay.com/photo/2021/08/22/08/10/hoi-an-6564496_960_720.jpg'),
                  NetworkImage(
                      'https://cdn.pixabay.com/photo/2020/12/09/09/27/women-5816861_960_720.jpg'),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                  left: smallScreenGrid ? 250.0 : 5.0,
                  top: extraLargeScreenGrid
                      ? 60.0
                      : largeScreenGrid
                          ? 50.0
                          : smallScreenGrid
                              ? 40.0
                              : tabScreenGrid
                                  ? 80.0
                                  : 80.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Nhiều thứ hấp dẫn đang chờ bạn',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: extraLargeScreenGrid
                              ? 30.0
                              : largeScreenGrid
                                  ? 27.0
                                  : smallScreenGrid
                                      ? 24.0
                                      : tabScreenGrid
                                          ? 20.0
                                          : 20.0,
                          fontWeight: FontWeight.w900),
                    ),
                    SizedBox(
                      height: 05.0,
                    ),
                    Text(
                      'Khám phá ngay!',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: extraLargeScreenGrid
                              ? 80.0
                              : largeScreenGrid
                                  ? 70.0
                                  : smallScreenGrid
                                      ? 60.0
                                      : tabScreenGrid
                                          ? 35.0
                                          : 35.0,
                          fontWeight: FontWeight.w900),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15.0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(0.0),
                          ),
                          padding:
                              EdgeInsets.all(smallScreenGrid ? 18.0 : 05.0),
                        ),
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginPage()));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: smallScreenGrid ? 40.0 : 18.0,
                              top: 02.0,
                              right: smallScreenGrid ? 40.0 : 18.0,
                              bottom: 02.0),
                          child: Text(
                            'Bắt đầu',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
