
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' ;
import 'package:travelmart/pages/details/detailsPage.dart';
import 'package:travelmart/pages/home/homePage.dart';
import 'dart:developer';

import 'package:travelmart/pages/login_register/loginPage.dart';
import 'package:travelmart/pages/login_register/registerPage.dart';
import 'package:travelmart/pages/profile/profilePage.dart';
import 'package:travelmart/pages/shopping/shoppingPage.dart';
void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Travelmart',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ProfilePage(),
    );
  }
}
